package com.vidatak.vidatalk.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.vidatak.vidatalk.Prefs;
import com.vidatak.vidatalk.R;
import com.vidatak.vidatalk.adapter.TileAdapter;
import com.vidatak.vidatalk.component.EqualSpaceItemDecoration;
import com.vidatak.vidatalk.component.RecyclerItemClickListener;
import com.vidatak.vidatalk.data.ButtonInfo;
import com.vidatak.vidatalk.database.ButtonsDatabase;
import com.vidatak.vidatalk.util.LogUtil;

import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends TranslationFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String ARG_SCREEN_ID = "screen_id";
    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private TileAdapter adapter;
    private List<ButtonInfo> buttonsForScreen;
    private TextView mainText;
    private TextView translationText;

    Configuration configuration;
    private Context mContext;

    private static final String tag = "VidaTalkHome";
    private int screenId = -1;
    private int mImageThumbSpacing = 0;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance ofint
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(int screenId) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SCREEN_ID, screenId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            screenId = getArguments().getInt(ARG_SCREEN_ID);
        }
        Locale locale = new Locale(Locale.getDefault().getLanguage());
        Locale.setDefault(locale);
        configuration = new Configuration();
//        configuration.locale = locale;
        mContext = getContext();
        getContext().getResources().updateConfiguration(configuration,
                getContext().getResources().getDisplayMetrics());

        mImageThumbSpacing = (int) getResources().getDimension(R.dimen.tile_separation);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new EqualSpaceItemDecoration(mImageThumbSpacing));

        //get buttons from database and set adapter
        final ButtonsDatabase buttonsDb = new ButtonsDatabase(getContext());
        buttonsDb.open();
        buttonsForScreen = buttonsDb.getButtonsForScreen(screenId, Prefs.getCurrentLocale(), Locale.getDefault(), Prefs.getCurrentGender());
        Log.d(tag, "there are " + buttonsForScreen.size() + " buttons");
        buttonsDb.close();

        Prefs.addListener(this);
        mainText = (TextView) v.findViewById(R.id.homeTxt);
        translationText = (TextView) v.findViewById(R.id.homeTxtTranslated);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Speak and set text
                        if (position >= 0 && position < buttonsForScreen.size()) {
                            ButtonInfo info = buttonsForScreen.get(position);
                            mListener.onButtonPressed(info, view, mainText, translationText);
                        }
                    }
                })
        );


        adapter = new TileAdapter(getContext(), buttonsForScreen, Prefs.getCurrentIcon());
        recyclerView.setAdapter(adapter);
        hideSoftKeyboard(v);
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(tag, "onAttach");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        adapter = new TileAdapter(getContext(), buttonsForScreen, Prefs.getCurrentIcon());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public TextView getMainTextView() {
        return mainText;
    }
    @Override
    public TextView getTranslationTextView() {
        return translationText;
    }

    @Override
    public void reload(Context c) {
        if (buttonsForScreen != null) {
            LogUtil.d(tag, "Reloading fragment " + screenId);
            ButtonsDatabase buttonsDb = new ButtonsDatabase(c);
            buttonsDb.open();
            buttonsForScreen.clear();
            buttonsForScreen.addAll(buttonsDb.getButtonsForScreen(screenId, Prefs.getCurrentLocale(), Locale.getDefault(),Prefs.getCurrentGender()));
            buttonsDb.close();
            adapter.notifyDataSetChanged();
        }
    }
}
