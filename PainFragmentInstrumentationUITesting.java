package com.vidatak.vidatalk;

/**
 * Created by ericecheverri on 1/25/16.
 */

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.not;



@RunWith(AndroidJUnit4.class)
@LargeTest
public class PainFragmentInstrumentationUITesting  {

    private String mApplicationString;
    private MainActivity mActivity;
    private EspressoHelpers espressoHelpers = new EspressoHelpers();

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule(MainActivity.class);

    @Before
    public void initValidString() {
        // Specify a valid string for the application
        mApplicationString = "VidaTalk";
    }

    @Test
    public void ApplicationRenders() {
        // Check that the text on the drawer is Vidatalk
        onView(withId(R.id.txtAppTitle)).check(matches(withText(mApplicationString)));
    }


    public void verifyOnClickPainFragmentHowBadIsThePain() {
        // Verify Pain fragment renders
        onView(withText("Pain")).perform(click());
//        onView(withId(R.id.nav_pain)).perform(click());
        onView (withId(R.id.btnContinue)).check(matches(isDisplayed()));
        onView (withId(R.id.btnPrev)).check(matches(not(isDisplayed())));
    }

    @Test
    public void VerifySwipingOnPainFragments(){
        // Goto first pain Fragment
        onView(withText("Pain")).perform(click());


        // Goto second fragment and check conditions
        onView(withId(R.id.painImage)).perform(swipeLeft());

        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.btnContinue)).check(matches(isDisplayed()));
        onView (withId(R.id.btnPrev)).check(matches(isDisplayed()));

        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Goto third fragment and check conditions
        onView(withId(R.id.bodyImage)).perform(swipeLeft());

        onView (withId(R.id.btnContinue)).check(matches(not(isDisplayed())));
        onView (withId(R.id.btnPrev)).check(matches(isDisplayed()));

        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Now go back
        //Grab the recycler view at the given index, and make sure all children match.
        onView(withId(R.id.recyclerView)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, swipeRight()));
        onView (withId(R.id.btnContinue)).check(matches((isDisplayed())));
        onView (withId(R.id.btnPrev)).check(matches(isDisplayed()));

        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Goto the first fragment
        onView(withId(R.id.bodyImage)).perform(swipeRight());
        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView (withId(R.id.btnContinue)).check(matches((isDisplayed())));
        onView (withId(R.id.btnPrev)).check(matches(not(isDisplayed())));

    }

    @Test
    public void VerifyClickingOnPainFragment(){
        // Goto first pain Fragment
        onView(withText("Pain")).perform(click());

        onView (withId(R.id.btnContinue)).check(matches((isDisplayed())));
        onView (withId(R.id.btnPrev)).check(matches(not(isDisplayed())));
        // Click on forward button on first fragment
        onView(withId(R.id.btnContinue)).perform(click());
        onView (withId(R.id.btnContinue)).check(matches((isDisplayed())));
        onView (withId(R.id.btnPrev)).check(matches(isDisplayed()));
        //  Click on forward button on Second fragment
        onView(withId(R.id.btnContinue)).perform(click());
        onView (withId(R.id.btnContinue)).check(matches(not((isDisplayed()))));
        onView (withId(R.id.btnPrev)).check(matches((isDisplayed())));
        //  Click on backward button on third fragment
        onView(withId(R.id.btnPrev)).perform(click());
        onView (withId(R.id.btnContinue)).check(matches((isDisplayed())));
        onView (withId(R.id.btnPrev)).check(matches(isDisplayed()));
        //  Click on backward button on second fragment
        onView(withId(R.id.btnPrev)).perform(click());
        onView (withId(R.id.btnContinue)).check(matches((isDisplayed())));
        onView (withId(R.id.btnPrev)).check(matches(not(isDisplayed())));

    }

    @Test
    public void VerifyScrollDownOnFragmentDescribeThePain(){
        // Goto first pain Fragment
        onView(withText("Pain")).perform(click());
        // Click on forward button on first fragment
        onView(withId(R.id.btnContinue)).perform(click());
        //  Click on forward button on Second fragment
        onView(withId(R.id.btnContinue)).perform(click());
        //Grab the recycler view last item and perform a scroll.
        //TODO Find last item in recyclerview instead of placing a number here

        onView(withId(R.id.recyclerView)).perform(
                RecyclerViewActions.actionOnItemAtPosition(15, scrollTo()));
    }

    @Test
    public void verifyPainMenuOptionRendersOnFirstFragment() {
        // Go to the second Fragment of nav_pain option
        onView(withText("Pain")).perform(click());

        onView(withId(R.id.painImage)).perform(swipeLeft());

        // Perform a click in another menu item and verify pain fragment does not render
        onView(withText("I Want")).perform(click());
        onView(withId(R.id.btnContinue)).check(doesNotExist());

        // Go back to Pain fragment and check the buttons assertions
        onView(withText("Pain")).perform(click());
        onView (withId(R.id.painImage)).check(matches(isDisplayed()));
        onView (withId(R.id.btnContinue)).check(matches(isDisplayed()));
        onView (withId(R.id.btnPrev)).check(matches(not(isDisplayed())));
    }

    @Test
    public void verifyTextRendersOnHowBadIsThePain() {
        // Verify Home fragment renders
        onView(withText("Pain")).perform(click());

        // Get any grid's text item
        String expectedGridItemName1 = "My pain is a 5";

        try {
            Thread.sleep(3000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.painImage)).perform(click());
        // Check that the text matches for that unique id
        onView(withId(R.id.translationTxt)).check(matches(withText(expectedGridItemName1)));
    }

    @Test
    public void verifyTextRendersOnWhereIsThePain() {
        // Verify Home fragment renders
        onView(withText("Pain")).perform(click());

        // Where is the pain
        onView(withId(R.id.btnContinue)).perform(click());
        // Get any grid's text item
        onView (withId(R.id.bodyImage)).check(matches(isDisplayed()));

        onView(withId(R.id.bodyImage)).perform(click());
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String expectedGridItemName2 = "I have pain in my left hand";
        // Check that the text matches for that unique id
        onView(withId(R.id.translationTxt)).check(matches(withText(expectedGridItemName2)));

    }

    @Test
    public void verifyTextRendersDescribeThePain() {
        // Verify Home fragment renders
        onView(withText("Pain")).perform(click());

        // Where is the pain
        onView(withId(R.id.btnContinue)).perform(click());

        // Describe the pain
        onView(withId(R.id.btnContinue)).perform(click());
        // Get any grid's text item
        String expectedGridItemName = "Aches";

        Matcher<View> recyclerView = espressoHelpers.withRecyclerView(R.id.recyclerView);

        //The child of the view this Matcher is passed into has the appropriate title in the title view.
        Matcher<View> hasTitleChildWithAppropriateContent = withChild(allOf(withId(R.id.tileText), withText(expectedGridItemName)));

        //Grab the recycler view at the given index, and make sure all children match.
        espressoHelpers.onRecyclerItemViewAtPosition(R.id.recyclerView, 0).check(matches(hasTitleChildWithAppropriateContent));

        onView(withId(R.id.recyclerView)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, click()));

        // Check that the text matches for that unique id
        onView(withId(R.id.translationTxt)).check(matches(withText("This part of my body aches")));
    }

}