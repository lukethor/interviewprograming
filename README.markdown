#  Android Google Play Vidatalk Application 
   
   See recent release in Google play market here :

  ![Vidatalk Android Market](http://www.droidaddiction.com/vidatalk.png)

[Vidatalk Android](https://play.google.com/store/apps/details?id=com.vidatak.vidatalk&hl=en)

### Robolectric headless Testing on Android


Robolectric is a testing framework that de-fangs the Android SDK so you can test-drive the development of your Android app [Robolectric Android](https://github.com/robolectric/robolectric)


### Screenshots, Videos

![Vidatalk Android Market](http://www.droidaddiction.com/roboelectic.png)


[DB Testing Video ](https://drive.google.com/file/d/0B9feydguSeEXalhJdkM1OVQ0MUU/view?usp=sharing)

### Using Espresso with ActivityTestRule

The Espresso testing framework, provided by the Android Testing Support Library, provides APIs for writing UI tests to simulate user interactions within a single target app. [Automating UI Testing](https://developer.android.com/training/testing/ui-testing/espresso-testing.html)

### Videos, Screenshots

![Vidatalk Android Market](http://www.droidaddiction.com/espresso.png)




### Application Component Fragments

A Fragment represents a behavior or a portion of user interface in an Activity. You can combine multiple fragments in a single activity to build a multi-pane UI and reuse a fragment in multiple activities. [Developer Fragments](https://developer.android.com/guide/components/fragments.html)


### Screenshots

![Vidatalk Android Market](http://www.droidaddiction.com/fragment.png)





