# Build Android AOSP using Ubuntu 14.04 LTS

   
   In general, building your own AOSP looks too complicated and it always seems like you will need to spend several weeks setting up the environment and compiling AOSP. Trying to follow the AOSP the official android guide  can be frustrating at times since some of the documentation is out of date. 


[AOSP Android Guide](http://source.android.com/source/requirements.html)

This guide will focus only in the following Android flavors:

![Android tag 59](http://www.droidaddiction.com/ubuntu4.png)

# GNU/Linux System


According to the guide, the following line says we need to choose an Ubuntu ISO image 14.04

	1. Android 6.0 (Marshmallow) - AOSP master: Ubuntu 14.04 (Trusty)


# Download Ubuntu ISO image


[Unix Flavor](http://releases.ubuntu.com/14.04/)


# JDK 7 for Ubuntu LTS 14.04


Here is where the developer make mistakes. Choose only the recommended JAVA version for the specific Android flavor.

```sh
sudo apt-get install openjdk-7-jre

sudo apt-get install openjdk-7-jdk

```

```sh
java -version
javac -version

```
![Java Version](http://www.droidaddiction.com/ubuntu3.png)



# Installing required packages (Ubuntu 14.04)


```sh

$ sudo apt-get install git-core gnupg flex bison gperf build-essential \
  zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 \
  lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache \
  libgl1-mesa-dev libxml2-utils xsltproc unzip


```


# Choosing a Branch


Since we already determined in the steps above Android 6.0 (Marshmallow), chose a 6.0 tag from your particular tablet from here :

[Tags](http://source.android.com/source/build-numbers.html)


For my Nexus Wifi tablet I have chosen :

![Android tag 59](http://www.droidaddiction.com/ubuntu5.png)

# Downloading the Source



* Create a working directory

```sh

$ mkdir ~/bin
$ PATH~/bin:$PATH

```

* Download the Repo tool and ensure that it is executable:

```sh

$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo

```

* Create an empty directory to hold your working files. If you're using MacOS, this has to be on a case-sensitive filesystem. Give it any name you like:

```sh
$ mkdir aosp
$ cd aosp

```

* Configure git

```sh

$ git config --global user.name "Your Name"
$ git config --global user.email "you@example.com"
```

* Run repo init t

```sh
 repo init -u https://android.googlesource.com/platform/manifest

```
check out a branch from the previous step 

```sh
 repo init -u https://android.googlesource.com/platform/manifest -b android-6.0.1_r59

```

# Repo synch takes too long


 Use this command to avoid long sync times

```sh

 repo sync -c --no-clone-bundle

```

# Set up environment
 

```sh

 $ source build/envsetup.sh

```

* Choose a target

```sh

 $lunch 

```
![lunch command](http://www.droidaddiction.com/ubuntu6.png)

# Build the code
 

```sh

 $ make -j4

```

# Go to sleep 
 

 I'm serious go to sleep. The previous command takes forever


# How do I know if the make command executed sucessfully
 
![make Command Execution](http://www.droidaddiction.com/ubuntu1.png)



# Look for AOSP images


```sh

 find . -name *.img

```

![flounder Image](http://www.droidaddiction.com/ubuntu2.png)

# Done

```sh

emulator&

```

 










